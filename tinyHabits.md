# Tiny Habits


## 1. Tiny Habits - BJ Fogg


### Q1. Your takeaways from the video

* Start with small habits that are easy to repeat consistently over time, rather than trying to achieve big goals all at once.
* To create a new habit, you need to be motivated to do it, it should be easy enough to do, and you should have a reminder or trigger to prompt you to do it.
* A good way to create a new habit is to attach it to something you already do regularly so that it becomes part of your existing routine.
* Instead of just focusing on the outcome you want to achieve, focus on the behaviors that will help you get there.
* Even small habits can add up and make a big difference over time, so keep doing them consistently to see results.


## 2. Tiny Habits by BJ Fogg - Core Message


### Q2. Your takeaways from the video in as much detail as possible

* The behavior is the combination of Motivation, Ability and Prompt.
  * Motivation - the desire to do the behavior.
  * Ability - the capacity to do the behavior.
  * Prompt - the cue to do the behavior.
* Shrink habits to the tiniest possible version so that very little motivation is required to do it.
*  Adopting a habit is easy but being consistent with it is hard.
* Three prompts trigger the habit.
    * External Prompt
    * Internal Prompt
    * Action Prompt
* The action prompt is the best way to adopt a new habit.
* Learning to celebrate after the accomplishment of work is the most critical component of habit development.


### Q3. How can you use B = MAP to make making new habits easier?

* Start with a small habit that requires less motivation to complete, like doing 5 minutes of exercise in the morning.



### Q4. Why it is important to "Shine" or Celebrate after each successful completion of a habit?

* Celebrating after completing a habit is important because it makes you feel good and motivated, which encourages you to continue and maybe even improve the habit.



## Q3. 1% Better Every Day Video


### Q5. Your takeaways from the video (Minimum 5 points)

* If you plan when and where you'll do something, you're more likely to do it.
* Clear instructions can help you feel motivated to do something.
* The places you go and the people you're with can affect what you feel like doing. You can try to set up your environment to help you do what you want.
* Doing something often is usually better than doing it perfectly but less often.
* To build a habit, do it every day and mark a calendar to keep track.


## 4. Book Summary of Atomic Habits

### Q6. Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

* Atomic habits are small actions or routines that we do regularly, which can have a big impact on our lives over time. 
* Bad habits often continue because of a flawed system, not just because we don't want to change.
*  Making small changes, even if they don't seem important at first, can lead to significant improvements in our lives if we stick with them for a long time.


### Q7. Write about the book's perspective on how to make a good habit easier?

* Make It Obvious, and make it easy to trigger.
* Make It Attractive.
* Make It Easy. Decrease the number of steps between you and your good habits.
* Make It Satisfying.


### Q8. Write about the book's perspective on making a bad habit more difficult?

* Make it invisible, make it harder to trigger.
* Keep more steps between you and the bad behavior.
* Make it unsatisfying.


## 5. Reflection


### Q9. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

* I would like to pick a habit of reading books every day.
* I will keep the book near my bed, so it will be easy access for me.


### 10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

* I will not use my smartphone just before sleep.
* By keeping my smartphone far away from my bed.