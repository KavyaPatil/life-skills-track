# Listening and Active Communication



1. Active Listening


    **Q1. What are the steps/strategies to do Active Listening?**


    - Stay focused on the conversation and don't let your thoughts distract you.
    
    - Don't interrupt the other person while they are speaking.
    
    - Use your body language to show that you are listening, like maintaining eye contact 
    
    and nodding.
    
    - Take notes if the conversation is important to help you remember important points.
    
    - Pay attention to the speaker and what they are talking about.
   
    - Use questions or statements that encourage the other person to keep talking.




2. Reflective Listening


    Reflective listening is used widely in different situations. It is a subset of Active 
    
    Listening with more emphasis on mirroring body language, emotions and verifying the 
    
    message.

    In critical situations e.g. conversations between Air Traffic Controller and Pilots, 
    
    conversations in military missions
   
    In business meetings - notes are taken by a notetaker and then verified by the 
    
    participants
    
    To diffuse emotionally charged conversations in personal relationships


    **Application to Business/Software Context**

    - Always take notes in meetings
    
    - Share the notes with all stakeholders - client, manager or team members and ask 
    
    them if there is a mistake
   
    - Make sure the team is on the same page i.e. everyone agrees on the points discussed
   
    - Take notes whenever you are in a technical discussion because there are minute 
    
    details that might slip from the mind when you get down to implementing.



    **Q2. According to Fisher's model, what are the key points of Reflective Listening?**


    - Pay attention to what the speaker is saying and think about what it means.
    
    - Take a moment to think about what you understood from the speaker's message, and 
    
    then repeat it back to them to make sure you understood correctly.
    
    - Listen more and talk less, so the speaker can share their thoughts and feelings.
    
    - Repeat and clarify what the other person said, without asking questions or sharing 
    
    your own thoughts, beliefs, or wants.
    
    - Respond with genuine care and understanding, not pretending or faking concern.
    
    - Try to understand how the speaker feels, in addition to what they are saying.




3. Reflection


    **Q3. What are the obstacles in your listening process?**


    - Noise and Distractions

    - Preconceived Ideas and Biases

    - Emotional Interference

    - Lack of Focus and Attention

    - Cultural and Language Barriers



    **Q4. What can you do to improve your listening?**


    - Be fully present in the conversation and avoid distractions.
    
    - Maintain eye contact with the speaker.
    
    - Don't interrupt; let the speaker finish before responding.
    
    - Use nonverbal cues like nodding and smiling to show you're listening.
    
    - Practice active listening by summarizing or paraphrasing the speaker's message.
    
    - Ask questions if you need clarification.
    
    - Stay focused and avoid internal distractions or forming responses in your mind.
    
    - Approach the conversation with an open mind, willing to consider different 
    
    perspectives.
    
    - Manage your emotions to stay attentive.



4. Types of Communication


    - Passive Communication
    
    - Aggressive Communication
    
    - Passive Aggressive Communication
    
    - Aggressive Communication



    **Q5. When do you switch to Passive communication style in your day to day life?**


    - If I am worried about something.
    
    - If the other person is not open to communication.

    - If the subjects the other person is talking does not interest me.
    


    **Q6. When do you switch into Aggressive communication styles in your day to day life?**


    - Never in Office or in friends circle

    - Mostly with family members for some personal reasons.





    **Q7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?**  

    
    Most of the times it is silent treatment from my side,

    - When other person annoys me for some reasons. 

    - When they don't respect my space.

    - When I am emotional about something.




     **Q8. How can you make your communication assertive?**



    - Maintain good eye contact during communication.
    
    - Before expressing yourself, identify and label your emotions.
    
    - Clearly, respectfully, and appropriately communicate your needs and desires.
    
    - Start practicing communication skills in less important or less stressful 
    
    situations.
    
    - Pay attention to your body language to ensure it aligns with your message.
    
    - Speak up promptly if someone treats you unfairly.


