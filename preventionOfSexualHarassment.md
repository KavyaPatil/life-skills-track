# Prevention of Sexual Harassment


## Kinds of behaviour to cause sexual harassment:


 - Physical contact or advances.

 - A  demand or request for sexual favours.

 - Making sexually coloured remarks.

 - Any other unwelcome physical, verbal or non-verbal conduct of a sexual nature.




## What would you do in case you face or witness any incident or repeated incidents of such behaviour?


 - If i feel threatened or unsafe, remove myself from the immediate situation and find a secure space.

 - Keep a record of the details surrounding the incidents, including dates, times, locations, 
 
    descriptions of the behavior, and any witnesses present.

 - Reach out to a supportive friend, family member, or colleague who can provide emotional support and 
 
    guidance. 

 - Familiarize yourself with the organization's or institution's policies and procedures on handling
 
    sexual harassment.

 - Report the incidents to the appropriate authorities within the organization or institution.

 - Reach out to support organizations or helplines that specialize in sexual harassment issues.