# SOLID Principles in Javascript

SOLID principles are a set of software designs introduced by Robert C. “Uncle Bob” Martin. These 

principles guide developers in building robust, maintainable applications while minimizing the cost of 

changes.



![SOLID PRINCIPLES](https://www.xenonstack.com/hubfs/xenonstack-solid-designs-principles.png)



### Derick’s definition of the five principles are:


1. **Single Responsibility Principle** 


    A class, a module, or a function should be only responsible for one actor. So, it should have one 
    
    and only one reason to change.


   **Example**

     Let's take a look at the following code:

    ```JavaScript

        class TodoList {
    constructor() {
        this.items = []
    }

    addItem(text) {
        this.items.push(text)
    }

    removeItem(index) {
        this.items = items.splice(index, 1)
    }

    toString() {
        return this.items.toString()
    }

    save(filename) {
        fs.writeFileSync(filename, this.toString())
    }

    load(filename) {
        // Some implementation
    }
    }

    ```

    The above code violates the Single responsibility principle. We added second responsibility to our 
    
    TodoList class which is the management of our database.

    To fix this issue, we have to separate the responsibility. Create a new class to manage the database.

 
    ```

    class DatabaseManager {
    saveToFile(data, filename) {
        fs.writeFileSync(filename, data.toString())
    }

    loadFromFile(filename) {
        // Some implementation
    }
    }


    ```



2. **Open Closed Principle**


   Functions, modules, and classes should be extensible but not modifiable. 


    **Example**

    ```
        class ManageSalaries {
    constructor() {
        this.salaryRates = [
        { id: 1, role: 'developer', rate: 100 },
        { id: 2, role: 'architect', rate: 200 },
        { id: 3, role: 'manager', rate: 300 },
        ];
    }

    calculateSalaries(empId, hoursWorked) {
        let salaryObject = this.salaryRates.find((o) => o.id === empId);
        return hoursWorked * salaryObject.rate;
    }
    }

    const mgtSalary = new ManageSalaries();
    console.log("Salary : ", mgtSalary.calculateSalaries(1, 100));

    ```

    Directly modifying the salaryRates array will violate the open-closed principle. For example, 
    
    suppose you need to extend the salary calculations for a new role. In that case, you need to 
    
    create a separate method to add salary rates to the salaryRates array without making to the 
    
    original code.


    ```

        class ManageSalaries {
    constructor() {
        this.salaryRates = [
        { id: 1, role: 'developer', rate: 100 },
        { id: 2, role: 'architect', rate: 200 },
        { id: 3, role: 'manager', rate: 300 },
        ];
    }

    calculateSalaries(empId, hoursWorked) {
        let salaryObject = this.salaryRates.find((o) => o.id === empId);
        return hoursWorked * salaryObject.rate;
    }

    addSalaryRate(id, role, rate) {
        this.salaryRates.push({ id: id, role: role, rate: rate });
    }
    }

    const mgtSalary = new ManageSalaries();
    mgtSalary.addSalaryRate(4, 'developer', 250);
    console.log('Salary : ', mgtSalary.calculateSalaries(4, 100));


    ```


3. **Liskov Substitution Principle**


   Objects of a superclass should be able to be replaced with objects of subclasses without causing 
   
   the application to break.


   **Example**

   ```

        class Animal{
    eat() {
        console.log("Animal Eats")
    }
    }

        class Bird extends Animal{
    fly() {
        console.log("Bird Flies")
    }
    }

    var parrot = new Bird();
    parrot.eat();
    parrot.fly();


        class Ostrich extends Bird{
    console.log("Ostriches Do Not Fly")
    }

    var ostrich = new Ostrich();
    ostrich.eat();
    ostrich.fly();


   ```


   This extension of the Bird class violates the Liskov principle since Ostriches cannot fly—this 
   
   could create unexpected behavior in the application. The best way to address this case is to extend 
   
   the Ostrich class from the Animal class.


4. **Interface Segregation Principle**


    "Clients should not be pushed to employ interfaces that they are unfamiliar with or they don't 
    
    want to use".

    **Note:** JS does not have an interface.



5.  **Dependency Inversion Principle**


    Higher-level modules should use abstractions. However, they should not depend on low-level 
    
    modules. 


     **Example** 

     ```

        class FileSystem {
    writeToFile(data) {
        // Implementation
    }
    }

    class ExternalDB {
    writeToDatabase(data) {
        // Implementation
    }
    }

    class LocalPersistance {
    push(data) {
        // Implementation
    }
    }

    class PersistanceManager {
    saveData(db, data) {
        if (db instanceof FileSystem) {
        db.writeToFile(data)
        }

        if (db instanceof ExternalDB) {
        db.writeToDatabase(data)
        }

        if (db instanceof LocalPersistance) {
        db.push(data)
        }
    }
    }


     ```

     In this case, a high-level module PersistanceManager depends on the low-level modules, which are 
     
     FileSystem, ExternalDB, and LocalPersistance.

        To avoid the issue in this simple case we should probably do something like this:


        ```
        class FileSystem {
    save(data) {
        // Implementation
    }
    }

    class ExternalDB {
    save(data) {
        // Implementation
    }
    }

    class LocalPersistance {
    save(data) {
        // Implementation
    }
    }

    class PersistanceManager {
    saveData(db, data) {
        db.save(data)
    }
    }


        ```





    **Referenses**

    1. [solid-principles-in-javascript](https://www.syncfusion.com/blogs/post/solid-principles-in-javascript.aspx)

    2. [solid-principles-javascript](http://www.xenonstack.com/blog/solid-principles-javascript)

    3. [5-solid-principles-with-javascript-how-to-make-your-code-solid](https://dev.to/denisveleaev/5-solid-principles-with-javascript-how-to-make-your-code-solid-1kl5)

    4. [Youtube](https://www.youtube.com/playlist?list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme)

