# Energy Management

## Question 1

**What are the activities you do that make you relax - Calm quadrant?**

- Relaxation
- Taking a walk
- Good structure and daily routine - having time for eating, sleeping, working and chilling
- Meditation

## Question 2

**When do you find getting into the Stress quadrant?**

- Tension
- Anger
- Fear
- Not able to complete the task assigned.

## Question 3

**How do you understand if you are in the Excitement quadrant?**

- Surprise
- Happiness
- Enjoyment
- Satisfaction

## Sleep is your superpower

## Question 4

**Paraphrase the Sleep is your Superpower video in detail.**

- Sleep is crucial for our brain and overall health. It helps us remember things better.
- Lack of sleep can lead to aging faster and increase the risk of Alzheimer's disease.
- Losing an hour of sleep in spring can cause more heart attacks, while gaining an hour in autumn   
   reduces heart attack risk.
- Sleep helps our body fight against viruses and cancer by strengthening the immune system.
- To improve sleep, maintain a regular sleep schedule and create a relaxing bedtime routine.
- Make sure your sleeping area is comfortable and cool.
- Avoid screens and caffeine close to bedtime for better sleep quality.

## Question 5

**What are some ideas that you can implement to sleep better?**

- Regularity
- Keep it cool (to fall asleep in a room that's too cold than too hot)

## Brain Changing Benefits of Exercise

## Question 6

**Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.**

- Prefrontal cortex, right behind your forehead,critical for things like decision-making, focus, attention and your personality.
- Hippocampus is a key structure critical for your ability to form and retain new long-term memories for facts and events.
- Exercise:
  - It has immediate effects on your brain.
  - You get better focus and attention.
  - Long-lasting increases in effects of mood.
- The rule of thumb is you want to get three to four times a week exercise(minimum 30 minutes an exercise session).

## Question 7

**What are some steps you can take to exercise more?**

- 30 minutes of exercise
- Add an extra walk, like taking stair case.
- Brigning some exercise into life.
