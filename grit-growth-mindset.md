# Grit and Growth Mindset


## 1. Grit

**Q1.**

- IQ is often used to measure intelligence, but it 
    
    doesn't guarantee success or performance.
   
- Grit refers to having passion and perseverance towards 
    
    achieving goals.
    
- Hard work and effort are essential for learning and 
    
    achieving success, regardless of IQ.
    
- Grit helps individuals throughout their lives to improve and grow.
   


**Q2.**

- Put in full effort when learning new things.
   
- Stay motivated and enthusiastic about the tasks at hand.
    
- Be willing to work hard to improve and achieve desired outcomes.


## 2. Introduction to Growth Mindset


**Q3.**

**Fixed Mindset:**

- Belief that abilities are fixed and cannot be changed.
    
- Skills and capabilities are seen as predetermined and 

    unchangeable.


**Growth Mindset:**

- Belief that abilities can be developed and improved.
    
- Skills and capabilities can be built through effort and 

    practice.

**Key Characteristics of a Growth Mindset:**

- Putting effort into learning and skill development.
    
- Embracing challenges as opportunities for growth.
    
- Accepting mistakes and using them as learning experiences.
    
- Being open to and welcoming feedback for improvement.


**Q4.**

- Maintain self-belief and have confidence in your abilities.
    
- Dedicate effort and hard work to achieve your goals.
    
- Focus on the process of learning and improving rather than 

just the end result.
    
- Embrace feedback as a valuable tool for growth and learning.
    
- Learn from mistakes and use them as opportunities for improvement.


## 3. Understanding Internal Locus of Control


**Q5.**

- Feel like you have the power to control your life and make 

    choices.
   
- Take action to solve the problems that come your way.
   
- Recognize and value yourself for the steps you take to 

    address those problems.


 **Q6.**

 - Believe in your ability to figure out things.

 - Question your assumptions.


 **Q7.**

 - When you face tough situations, believe in yourself and 
 
   know that you can find a way to overcome them.
    
 - Don't give up if you're struggling with something. The 
 
 challenges you face can make you stronger and prepare you 
 
    for future obstacles.


## 4. Mindset - A MountBlue Warrior Reference Manual


 **Q8.**

 - I won't give up on a problem until I finish it completely.
    
 - I will make sure to understand each concept well.
   
 - I know that putting in more effort will help me understand things better.